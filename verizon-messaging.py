#!/usr/bin/python

import requests
import re
import sys
import json
import time


"""

This is a preliminary script for prototyping the idea of an abstraction layer for third-party messaging applications.

"""

## Configuration

# ... should be your mobile number 
account_userid = "3215551212"

# ... and your magic password
account_password = "passw3rd"


## Begin functional code

# ... initialize a Requests session, to store cookies and headers
s = requests.session()
s.headers.update({
    'Referer': 'https://login.vzw.com/cdsso/public/controller?action=logout',
    'User-Agent': 'Mozilla/5.0 (Macintosh Intel Mac OS X 10_8_3) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.172 Safari/537.22',
})

# ... sign in
r = s.request("POST", 'https://login.verizonwireless.com:443/amserver/UI/Login',
               allow_redirects=True,
               data={
                 'realm': 'vzw',
                 'goto': '',
                 'gx_charset': 'UTF-8',
                 'rememberUserNameCheckBoxExists': 'Y',
                 'login_select': '1',
                 'IDToken1': account_userid,
                 'IDToken2': account_password,
                 'rememberUserName': 'Y',
                 'signIn': '',
                 'fake_pass': account_userid,
                }
)


# ... update the headers
s.headers.update({ 'Host': 'web.vma.vzw.com' });


# ... go to Message.do
r = s.request("GET", 'https://web.vma.vzw.com/vma/web2/Message.do?showtc=true&n=1',
               allow_redirects=False,
               data={}
)


# ... upedate referrer header
s.headers.update({ 'Referer': 'https://web.vma.vzw.com/vma/web2/Message.do?showtc=true&n=1',  'Host': 'im1.vma.vzw.com' });



# !!! this is where it stops working, but we are definitely logged in at this point

# ... go to Core.do
r = s.request("GET", 'https://im1.vma.vzw.com:443/im/Core.do',
               allow_redirects=False,
               data={ 'mdn': account_userid }, 
               verify=False
)

print r.content
print r.url
print "-------------------------"


refurl = r.url
s.headers.update({ 
  'Referer': refurl,
  'Host': 'im1.vma.vzw.com',
  'X-Requested-With': 'XMLHttpRequest'
  });

r = s.request("GET", 'https://im1.vma.vzw.com:443/im/Init?hibmtn0',
               allow_redirects=False,
               data={ 'jsonData': '{"from":'+account_userid+',"wsid":"QJNLkSC4FLOVgBVk1FkO6IFQfk9obFg43fSCx4pKZULxr2ebaOJy8lH/KtrdiTFEvDbahFZMpFPT9VqoQ0XnME7mNTeqGxWje1luZ5hwxKZ9n/FHFvKPqWLIkPQ4UU3CB4U0e9lWFZAbOUwtx7mKMw==","rid":"459581122","cid":"m/VA1I+l2XkDeX6Zm63cUdx5o0g4iEx2RBBx0xW/s663i30rUoQ85rvWjesEQR28fiOxiDXNZOAzGrmmNUK1fNSaOY5tG18M2j3sjKzW8GUx5CvuYrJTJSJ4by2lGw/mJ6kuVO+wftVb4IIKs/ttsQ==","loadAutoReplyFlag":true,"reloadAddressBook":true,"type":"INIT_BOSH_SESSION","ver":"0001"}' }, 
               verify=False
)

print r.content
print r.url
print "-------------------------"

refurl = r.url
s.headers.update({ 'Referer': refurl, 'Host': 'im1.vma.vzw.com' });
r = s.request("GET", 'https://im1.vma.vzw.com:443/im/Poll?hibmtn1',
               allow_redirects=False,
               data={
                 'jsonData': '{"from":"'+account_userid+'","wsid":"QJNLkSC4FLOVgBVk1FkO6IFQfk9obFg43fSCx4pKZULxr2ebaOJy8lH/KtrdiTFEvDbahFZMpFPT9VqoQ0XnME7mNTeqGxWje1luZ5hwxKZ9n/FHFvKPqWLIkPQ4UU3CB4U0e9lWFZAbOUwtx7mKMw==","sid":"ZtSWZBn/qS/ci67ATVwK966M18mxQ9wbQ22N6k/Kw5zCb1SkJdKRcJ9JVUaCwD1J34nvMK0rmR27nvJ6PirY9YRReely+TNLGo6MqY7fXIdxVqVNaYvXeKVT4UAQd1FVgP+KPqHxx0rN0+30UrH5Mbxfi0Ihbk8+1ITqq+1F4cLVUO15k2eMAkh49Ho58ldaVH1XIjLDpRExTODQc0HwzzsQ/hL2LiZRNCmVJmwctEBF/X9jT5rc/CPwAHkrlau+tFymxme2OZTN8vQ2q5KxNbfiGAobpOtnL1L24VivBAsjs3g0NseFGdn9Z7vhmxCrRREbbXCrXLnwiOoNWERVDlWiMfaT4cs2uepOu7ucV0T4Yq/WuQ9jHkykIVXpZwOB7sAhjmLspt8vz7t2C31IFbjKMmQ/zUCPxKIC9bH6VIwgVIaRh5vv626eTnWnwwsm3PWqzdsl9UZPT5iglPtXkiepLlTvsH7VW+CCCrP7bbE=","rid":459581252,"cid":"m/VA1I+l2XkDeX6Zm63cUdx5o0g4iEx2RBBx0xW/s663i30rUoQ85rvWjesEQR28fiOxiDXNZOAzGrmmNUK1fNSaOY5tG18M2j3sjKzW8GUx5CvuYrJTJSJ4by2lGw/mJ6kuVO+wftVb4IIKs/ttsQ==","ver":"0001","type":"REQ_IN"}'}, 
                verify=False
)


print r.content
print r.url
print "-------------------------"


print "Done!"