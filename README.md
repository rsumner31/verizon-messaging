The aim of this project is to provide a library for accessing the Verizon Wireless Advanced Messaging system programmatically, 
and eventually developing plugins for third-party IM applications that allow you to send and receive SMS messages using 
your real mobile number, while still receiving them on your mobile device.

Verizon Wireless Advanced Messaging is a service that allows you to send/receive SMS/MMS over the internet using your existing mobile number.

You can see VAM in action at:
https://login.verizonwireless.com/amserver/UI/Login

They also have applications for Android and iOS.

